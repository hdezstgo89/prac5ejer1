package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.CuentaBancaria;

public class Myapp {

	public static void run() {
		CuentaBancaria c = new CuentaBancaria(17855645L,"Ricado Hernandez",50000F,LocalDate.of(2021, 5, 20));
		System.out.println(c);
		System.out.println(c.Retiro(25000F, LocalDate.of(2021, 6, 20)));
		System.out.println(c);
		System.out.println(c.Deposito(30000F, LocalDate.of(2021, 6, 20)));
		System.out.println(c);
		
		System.out.println();
		System.out.println();
		
		CuentaBancaria c1 = new CuentaBancaria(17855645L,"Ricado Hernandez",50000F,null);
		System.out.println(c1);
		System.out.println(c1.Retiro(75000F, LocalDate.of(2021, 6, 20)));
		System.out.println(c1);
		System.out.println(c1.Deposito(30000F, LocalDate.of(2021, 6, 21)));
		System.out.println(c1);
		
		System.out.println();
		System.out.println();

		CuentaBancaria c2 = new CuentaBancaria(17855645L,"Ricado Hernandez",70000F,LocalDate.of(2021, 5, 20));
		System.out.println(c2);
		System.out.println(c2.Retiro(25000F, LocalDate.of(2021, 6, 20)));
		System.out.println(c2);
		System.out.println(c2.Deposito(30000F, LocalDate.of(2021, 6, 21)));
		System.out.println(c2);
	}
	public static void main(String [] args) {
		run();
	}
}
