package ito.poo.clases;

import java.time.LocalDate;

public class CuentaBancaria {

	private long numCuenta = 0L;
	private String nomCliente = "";
	private float saldo = 0F;
	private LocalDate fechaApertura = null;
	private LocalDate fechaActualizacion = null;
	/*********************/
	public CuentaBancaria() {
		super();
	}
	public CuentaBancaria(long numCuenta, String nomCliente, float saldo, LocalDate fechaApertura) {
		super();
		this.numCuenta = numCuenta;
		this.nomCliente = nomCliente;
		this.saldo = saldo;
		this.fechaApertura = fechaApertura;
	}
	/*********************/
	public boolean Deposito(float Cantidad,LocalDate newfechaActualizacion) {
		boolean Deposito = false;
		if(this.fechaApertura==null)
			System.out.println("La cuenta no esta activa");
		else {
			Deposito = true;
			this.setSaldo(this.getSaldo()+ Cantidad);
			this.setFechaActualizacion(newfechaActualizacion);
		}
		return Deposito;
	}

	public boolean Retiro (float Cantidad,LocalDate newFechaActualizacion) {
		boolean Retiro = false;
		if (Cantidad <= this.getSaldo()) {
			 Retiro=true;
			 this.setSaldo(this.getSaldo()-Cantidad);
			 this.setFechaActualizacion(newFechaActualizacion);
			}
		else
			System.out.println("La cantidad a retirar sobrepasa el saldo");
		return Retiro;
	}
	/*********************/
	public long getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(long numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getNomCliente() {
		return nomCliente;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public LocalDate getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public LocalDate getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(LocalDate fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	/*********************/
	@Override
	public String toString() {
		return "CuentaBancaria [numCuenta=" + numCuenta + ", nomCliente=" + nomCliente + ", saldo=" + saldo
				+ ", fechaApertura=" + fechaApertura + ", fechaActualizacion=" + fechaActualizacion + "]";
	}
}